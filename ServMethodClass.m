//
//  ServMethodClass.m
//  MESISchedule
//
//  Copyright (c) 2015 MESI. All rights reserved.
//

#import "ServMethodClass.h"
#import "Connection.h"


@implementation ServMethodClass

#pragma mark - Request Group and Lecture

-(NSDictionary *)requestFirstGroup:(NSString *)group1 secondGroup:(NSString*)group2 thirdGroup:(NSString*)group3 {
    if (![self checkConnection]) {
        NSDictionary *noConnection = [[NSDictionary alloc] initWithObjectsAndKeys:@"Нет подключения.", @"Error", nil];
        return noConnection;
    }
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    NSString *stringURL;
    NSLog(@"group3 = %@", group3);
    if ([group2 isEqualToString:@""]) {
        group2 = @"group2";
        
    }
    if ([group3 isEqualToString:@""]) {
        
        group3 = @"group3";
        
    }
    stringURL = [NSString stringWithFormat:@"http://schedule.mesilab.ru/groups/p=%@?%@?%@", group1, group2, group3];
    NSLog(@"stringurl = %@", stringURL);
    NSString *webURL = [stringURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [request setURL:[NSURL URLWithString:webURL]];
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/json;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    
    NSURLResponse *responce;
    NSData *GETReply = [NSURLConnection sendSynchronousRequest:request returningResponse:&responce error:nil];
    NSString *theReply = [[NSString alloc] initWithBytes:[GETReply bytes] length:[GETReply length] encoding:NSASCIIStringEncoding];
    NSData *data = [theReply dataUsingEncoding:NSUTF8StringEncoding];
    NSError *errorMessage;
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorMessage];
    if ([json objectForKey:@"0"] || [json objectForKey:@"1"]) {
        return json;
    }
    NSDictionary *error = [[NSDictionary alloc] initWithObjectsAndKeys:@"Сервер не отвечает.", @"Error", nil];
    return error;
    
};


-(NSDictionary*)requstLecture:(NSString*)lecture{
    if (![self checkConnection]) {
        NSDictionary *noConnection = [[NSDictionary alloc] initWithObjectsAndKeys:@"Нет подключения.", @"Error", nil];
        return noConnection;
    }
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    NSString *stringURL = [NSString stringWithFormat:@"http://schedule.mesilab.ru/lectures/p=%@", lecture];
    NSLog(@"stringurl = %@", stringURL);
    NSString *webURL = [stringURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [request setURL:[NSURL URLWithString:webURL]];
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/json;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    
    NSURLResponse *responce;
    NSData *GETReply = [NSURLConnection sendSynchronousRequest:request returningResponse:&responce error:nil];
    NSString *theReply = [[NSString alloc] initWithBytes:[GETReply bytes] length:[GETReply length] encoding:NSASCIIStringEncoding];
    NSData *data = [theReply dataUsingEncoding:NSUTF8StringEncoding];
    NSError *errorMessage;
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorMessage];
    if ([json objectForKey:@"0"] || [json objectForKey:@"1"]) {
        return json;
    }
    NSDictionary *error = [[NSDictionary alloc] initWithObjectsAndKeys:@"Сервер не отвечает.", @"Error", nil];
    return error;
}

#pragma mark - Show lists groups and lectures


-(NSDictionary*)getListOfAll:(TypeOfList)Type{
    if (![self checkConnection]) {
        NSDictionary *noConnection = [[NSDictionary alloc] init];
        return noConnection;
    }
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    NSString *stringURL;
    switch (Type) {
        case groups:
            stringURL = [NSString stringWithFormat:@"http://schedule.mesilab.ru/lists/groups"];
            break;
            
        case lectures:
            stringURL = [NSString stringWithFormat:@"http://schedule.mesilab.ru/lists/lectures"];
            break;
    }
    NSLog(@"stringurl = %@", stringURL);
    NSString *webURL = [stringURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [request setURL:[NSURL URLWithString:webURL]];
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/json;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    
    NSURLResponse *responce;
    NSData *GETReply = [NSURLConnection sendSynchronousRequest:request returningResponse:&responce error:nil];
    NSString *theReply = [[NSString alloc] initWithBytes:[GETReply bytes] length:[GETReply length] encoding:NSASCIIStringEncoding];
    NSData *data = [theReply dataUsingEncoding:NSUTF8StringEncoding];
    NSError *errorMessage;
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorMessage];
    if ([json objectForKey:@"0"]) {
        return json;
    }
    NSDictionary *error = [[NSDictionary alloc] initWithObjectsAndKeys:@"Сервер не отвечает.", @"Error", nil];
    return error;
}

#pragma mark - Connection

- (BOOL) checkConnection {
    Connection *networkReachability = [Connection reachabilityForInternetConnection];
    if (![networkReachability isReachable])  {
        NSLog(@"No internet");
        return 0;
    } else  {
        NSLog(@"We have an internet");
        
        return 1;
    }
}




@end