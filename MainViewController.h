//
//  MainViewController.h
//  MESISchedule
//
//  Created by Виктор Киселев on 25.02.15.
//  Copyright (c) 2015 MESI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainViewController : UIViewController <UIPageViewControllerDataSource, UIPageViewControllerDelegate>

@property NSUInteger pageIndex;
@property (strong, nonatomic) UIPageViewController *selectedWeekPageViewController;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *barButton;

@property (nonatomic, strong) UITapGestureRecognizer *tapGestureRecognizer;



- (void) revealMenu;

//@property (strong,nonatomic) NSArray *weekTitles;

//@property (strong,nonatomic) ;




@end
