//
//  ServMethodClass.h
//  MESISchedule
//
//  Created by Виктор Киселев on 19.02.15.
//  Copyright (c) 2015 MESI. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    lectures,
    groups
} TypeOfList;

@interface ServMethodClass : NSObject

-(NSDictionary *)requestFirstGroup:(NSString *)group1 secondGroup:(NSString*)group2 thirdGroup:(NSString*)group3;
-(NSDictionary*)requstLecture:(NSString*)lecture;
-(NSDictionary*)getListOfAll:(TypeOfList)Type;
- (BOOL) checkConnection;

@end
