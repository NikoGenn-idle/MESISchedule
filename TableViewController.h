//
//  TableViewController.h
//  MESISchedule
//
//  Created by Reutskiy Jury on 03.04.15.
//  Copyright (c) 2015 MESI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableViewController : UITableViewController <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView *raspUpTable;
@property (strong, nonatomic) IBOutlet UITableView *raspLowTable;


@end
