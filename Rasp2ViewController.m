//
//  Rasp2ViewController.m
//  MESISchedule
//
//  Created by Виктор Киселев on 05.03.15.
//  Copyright (c) 2015 MESI. All rights reserved.
// ДКП-141п

#import "Rasp2ViewController.h"
#import "SubjTableViewCell.h"
#import "SWRevealViewController.h"

@interface Rasp2ViewController (){
    NSUserDefaults *user;
}

@end

@implementation Rasp2ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [_Rasp2Table setDelegate:self];
    [_Rasp2Table setDataSource:self];
    

    
    
    // Do any additional setup after loading the view.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *dict = [user valueForKey:@"raspDownWeek"];
    return [dict[section] count];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 6;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *simpleTableIndetfier = @"Subject";
    SubjTableViewCell *cell = (SubjTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIndetfier];
    NSMutableDictionary *resultWeek = [user valueForKey:@"raspDownWeek"];
    NSMutableDictionary *resultDay = [resultWeek valueForKey:[NSString stringWithFormat:@"%ld", (long)indexPath.section]];
    NSMutableDictionary *result = [resultDay objectForKey:[NSString stringWithFormat:@"%ld", (long)indexPath.row]];
//    [cell.timeLabel setText:[result valueForKey:@"TimeName"]];
    [cell.subjectLabel setText:[result valueForKey:@"SubjectName"]];
    [cell.lectureLabel setText:[result valueForKey:@"LectureName"]];
    NSString *classroom = [result valueForKey:@"ClassroomName"];
    classroom = [classroom stringByAppendingString:@"/"];
    classroom = [classroom stringByAppendingString:[result valueForKey:@"BuildingName"]];
    [cell.classroomLabel setText:classroom];
    [cell.typeLabel setText:[result valueForKey:@"TypeName"]];

    
    
    
    
    return cell;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
