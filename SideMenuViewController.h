//
//  SideMenuViewController.h
//  MESISchedule
//
//  Created by Reutskiy Jury on 21.04.15.
//  Copyright (c) 2015 MESI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SideMenuViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UIView *upBackgroundView;
@property (weak, nonatomic) IBOutlet UIImageView *logo;



@property (weak, nonatomic) IBOutlet UIImageView *personalImage;
@property (weak, nonatomic) IBOutlet UIImageView *calendarImage;
@property (weak, nonatomic) IBOutlet UIImageView *lectureImage;
@property (weak, nonatomic) IBOutlet UIImageView *exitImage;



@property (weak, nonatomic) IBOutlet UIButton *personalScheduleButton;
@property (weak, nonatomic) IBOutlet UIButton *calendarButton;
@property (weak, nonatomic) IBOutlet UIButton *lectureButton;
@property (weak, nonatomic) IBOutlet UIButton *exitButton;



- (IBAction)myScheduleAction:(UIButton *)sender;
- (IBAction)allGroupScheduleAction:(UIButton *)sender;
- (IBAction)lectureScheduleAction:(UIButton *)sender;
- (IBAction)changeGroupAction:(UIButton *)sender;





@end
