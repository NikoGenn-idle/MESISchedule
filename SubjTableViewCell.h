//
//  SubjTableViewCell.h
//  MESISchedule
//
//  Created by Виктор Киселев on 19.02.15.
//  Copyright (c) 2015 MESI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SubjTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *timeBeginLabel;
@property (strong, nonatomic) IBOutlet UILabel *timeEndLabel;


@property (strong, nonatomic) IBOutlet UILabel *subjectLabel;

@property (strong, nonatomic) IBOutlet UILabel *lectureLabel;

@property (strong, nonatomic) IBOutlet UILabel *classroomLabel;

@property (strong, nonatomic) IBOutlet UIView *background;

@property (strong, nonatomic) IBOutlet UILabel *typeLabel;


@property (weak, nonatomic) IBOutlet UITableView *cellTable;

@end
