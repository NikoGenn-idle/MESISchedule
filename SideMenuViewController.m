//
//  SideMenuViewController.m
//  MESISchedule
//
//  Created by Reutskiy Jury on 21.04.15.
//  Copyright (c) 2015 MESI. All rights reserved.
//

#import "SideMenuViewController.h"
#import "MenuTableViewCell.h"

@interface SideMenuViewController () {
    NSArray *iconArray;
    NSArray *menuItemNameArray;
    MenuTableViewCell *cell;
    NSUserDefaults *user;
}

@end

@implementation SideMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    user = [NSUserDefaults standardUserDefaults];

    self.upBackgroundView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"blueMenu2"]];
    iconArray = @[[UIImage imageNamed:@"pers_b"], [UIImage imageNamed:@"pers_g"], [UIImage imageNamed:@"calendar_b"], [UIImage imageNamed:@"calendar_g"], [UIImage imageNamed:@"prep_b"], [UIImage imageNamed:@"prep_g"], [UIImage imageNamed:@"exit_b"], [UIImage imageNamed:@"exit_g"]];
    menuItemNameArray = @[@"Моё расписание", @"Расисание групп", @"Расписание преподователя", @"Сменить группу"];
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) setAllIconsGrey {
    self.personalImage.image = iconArray[1];
    self.calendarImage.image = iconArray[3];
    self.lectureImage.image = iconArray[5];
    self.exitImage.image = iconArray[7];
}


#pragma mark - Segue

//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
//    
//    // Set the title of navigation bar by using the menu items
//    NSIndexPath *indexPath = [self.menuTable indexPathForSelectedRow];
//    UINavigationController *destViewController = (UINavigationController*)segue.destinationViewController;
////    destViewController.title = [[menuItemNameArray objectAtIndex:indexPath.row] capitalizedString];
//    
//    // Set the photo if it navigates to the PhotoView
//    if ([segue.identifier isEqualToString:@"showPhoto"]) {
//        UINavigationController *navController = segue.destinationViewController;
//        PhotoViewController *photoController = [navController childViewControllers].firstObject;
//        NSString *photoFilename = [NSString stringWithFormat:@"%@_photo", [menuItems objectAtIndex:indexPath.row]];
//        photoController.photoFilename = photoFilename;
//    }
//}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Actions


- (IBAction)myScheduleAction:(UIButton *)sender {
    
    [self setAllIconsGrey];
    self.personalImage.image = iconArray[0];

}

- (IBAction)allGroupScheduleAction:(UIButton *)sender {
    
    [self setAllIconsGrey];
    self.calendarImage.image = iconArray[2];

}

- (IBAction)lectureScheduleAction:(UIButton *)sender {
    
    [self setAllIconsGrey];
    self.lectureImage.image = iconArray[4];
    
}

- (IBAction)changeGroupAction:(UIButton *)sender {
    
    [self setAllIconsGrey];
    self.exitImage.image = iconArray[6];
    
    [user removeObjectForKey:@"raspUpWeek"];
    [user removeObjectForKey:@"raspDownWeek"];
}
@end


