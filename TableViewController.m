//
//  TableViewController.m
//  MESISchedule
//
//  Created by Reutskiy Jury on 03.04.15.
//  Copyright (c) 2015 MESI. All rights reserved.
//

#import "TableViewController.h"
#import "SubjTableViewCell.h"

@interface TableViewController () {
    NSUserDefaults *user;
}

@end

@implementation TableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [self.raspUpTable setDelegate:self];
    [self.raspUpTable setDataSource:self];
    
    [self.raspLowTable setDelegate:self];
    [self.raspLowTable setDataSource:self];
    
    user = [NSUserDefaults standardUserDefaults];

    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    NSString *title;
    switch (section) {
        case 0:
            title = @"Понедельник";
            break;
        case 1:
            title = @"Вторник";
            break;
        case 2:
            title = @"Среда";
            break;
        case 3:
            title = @"Четверг";
            break;
        case 4:
            title = @"Пятница";
            break;
        case 5:
            title = @"Суббота";
            break;
        default:
            break;
    }
    return title;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 6;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    //    if ([tableView isEqual:self.raspUpTable]) {
    NSDictionary *dict = [user objectForKey:@"raspUpWeek"];
    NSInteger rows = [[dict objectForKey:[NSString stringWithFormat:@"%d", section]] count];
    return rows;
    //    } else {
    //        NSDictionary *dict = [user objectForKey:@"raspLowWeek"];
    //        NSInteger rows = [[dict objectForKey:[NSString stringWithFormat:@"%d", section]] count];
    //        return rows;
    //    }
    
}



-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    //    if ([tableView isEqual:self.raspUpTable]) {
    static NSString *simpleTableIndetfier = @"Subject";
    SubjTableViewCell *cell = (SubjTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIndetfier];
    NSMutableDictionary *resultWeek = [user objectForKey:@"raspUpWeek"];
    NSLog(@"%@", resultWeek);
    NSMutableDictionary *resultDay = [resultWeek valueForKey:[NSString stringWithFormat:@"%ld", (long)indexPath.section]];
    NSMutableDictionary *result = [resultDay valueForKey:[NSString stringWithFormat:@"%ld", (long)indexPath.row]];
    NSLog(@"%@", result);
//    [cell.timeLabel setText:[result valueForKey:@"TimeName"]];
    [cell.subjectLabel setText:[result valueForKey:@"SubjectName"]];
    [cell.lectureLabel setText:[result valueForKey:@"LectureName"]];
    NSString *classroom = [result valueForKey:@"ClassroomName"];
    classroom = [classroom stringByAppendingString:@"/"];
    classroom = [classroom stringByAppendingString:[result valueForKey:@"BuildingName"]];
    [cell.classroomLabel setText:classroom];
    [cell.typeLabel setText:[result valueForKey:@"TypeName"]];
    
    return cell;
    //    } else {
    //
    //        static NSString *simpleTableIndetfier = @"Subject";
    //        SubjTableViewCell *cell = (SubjTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIndetfier];
    //        NSMutableDictionary *resultWeek = [user valueForKey:@"raspDownWeek"];
    //        NSMutableDictionary *resultDay = [resultWeek valueForKey:[NSString stringWithFormat:@"%ld", (long)indexPath.section]];
    //        NSMutableDictionary *result = [resultDay objectForKey:[NSString stringWithFormat:@"%ld", (long)indexPath.row]];
    //        [cell.timeLabel setText:[result valueForKey:@"TimeName"]];
    //        [cell.subjectLabel setText:[result valueForKey:@"SubjectName"]];
    //        [cell.lectureLabel setText:[result valueForKey:@"LectureName"]];
    //        NSString *classroom = [result valueForKey:@"ClassroomName"];
    //        classroom = [classroom stringByAppendingString:@"/"];
    //        classroom = [classroom stringByAppendingString:[result valueForKey:@"BuildingName"]];
    //        [cell.classroomLabel setText:classroom];
    //        [cell.typeLabel setText:[result valueForKey:@"TypeName"]];
    //
    //
    //        return cell;
    //    
    //    }
    
    
}

@end
