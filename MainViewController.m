//
//  MainViewController.m
//  MESISchedule
//
//  Created by Виктор Киселев on 25.02.15.
//  Copyright (c) 2015 MESI. All rights reserved.
//

#import "MainViewController.h"
#import "SWRevealViewController.h"
#import "RaspViewController.h"
#import "CalendarAndDate.h"


@interface MainViewController () {

    NSInteger index;
    NSInteger countIndexPage;
    NSString *firstController;
    NSString *secondController;
}

@end

@implementation MainViewController


-(void)viewDidLoad {
    [super viewDidLoad];
    
    CalendarAndDate *calendar = [[CalendarAndDate alloc] init];
    BOOL pairity = [calendar isUpWeekWithCurrentDate];
    if ([calendar isUpWeekWithCurrentDate]) {
        firstController = @"DownWeek";
        secondController = @"UpWeek";
    } else {
        firstController = @"UpWeek";
        secondController = @"DownWeek";
    }

    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController ) {
        [self.barButton setTarget: self.revealViewController];
        [self.barButton setAction: @selector(revealToggle:)];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    
//    [_SelectWeekPageViewController setDelegate:self];
    [self.selectedWeekPageViewController setDataSource:self];
    
//    _weekTitles = @[@"Верхняя Неделя", @"Нижняя Неделя"];
    
    
    
    self.selectedWeekPageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageViewController"];
    
    self.selectedWeekPageViewController.dataSource = self;
    
    index = 0;
    countIndexPage = 0;
    
//    [self.selectedWeekPageViewController initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationVertical options:nil];
    
    
    
//    RaspViewController *startingViewController = [self viewControllerAtIndex:0];
//    RaspViewController *secondViewController = [self viewControllerAtIndex:1];
    
    RaspViewController *startingViewController = [self.storyboard instantiateViewControllerWithIdentifier:firstController];
    
    NSArray *viewControllers = @[startingViewController];
    

    [self.selectedWeekPageViewController setViewControllers:viewControllers  direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    
    self.selectedWeekPageViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    
//    [self addChildViewController:_SelectWeekPageViewController];
    
    [self.view addSubview:self.selectedWeekPageViewController.view];
    self.view.alpha = 1;
//    [self.SelectWeekPageViewController didMoveToParentViewController:self];
    

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}





#pragma mark - Page View Controller Data Source




- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    
    

    
//    NSLog(@"Before begin with index = %d, and count = %d", index, countIndexPage);

    if (countIndexPage < 2) {
        countIndexPage++;
    } else {
        index = 0;
    }
    if (index == 1) {
        
        RaspViewController *UpWeekViewController = [self.storyboard instantiateViewControllerWithIdentifier:firstController];

        return UpWeekViewController;
        
    
    } else {
//        NSLog(@"Before end with index = %d, and count = %d return NIL", index, countIndexPage);

        return nil;
    }

}



- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
    

    
//    NSLog(@"After begin with index = %d, and count] = %d", index, countIndexPage);

    if (countIndexPage < 2) {
        countIndexPage++;
    } else {
        index = 1;
    }
    
    if (index == 0) {
        
//        if (countIndexPage <= 2) {
//            countIndexPage++;
//        } else {
//            index = 1;
//        }
        
        RaspViewController *DownWeekViewController = [self.storyboard instantiateViewControllerWithIdentifier:secondController];
//        index = 1;
        NSLog(@"After end with index = %d, and count = %d return view", index, countIndexPage);

        return DownWeekViewController;
        
    } else {
        NSLog(@"After end with index = %d, and count = %d return NIL", index, countIndexPage);

        return nil;
    }
}







-(NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController{
    return 0;
}
-(NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController{
    return 0;
}



- (void) revealMenu {
    [self.revealViewController revealToggleAnimated:YES];
    
    NSLog(@"And here roconized");
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
