//
//  RaspViewController.m
//  MESISchedule
//
//  Created by Виктор Киселев on 19.02.15.
//  Copyright (c) 2015 MESI. All rights reserved.
//

#import "RaspViewController.h"
#import "SubjTableViewCell.h"
#import "MainViewController.h"
#import "SWRevealViewController.h"
#import "CalendarAndDate.h"
#import "NullTableViewCell.h"


@interface RaspViewController ()

@end

@implementation RaspViewController {
    NSUserDefaults *user;
    UITableView *tableview;
    NSMutableArray *arrayWithDates;
    int currentDay;
    BOOL flagNullCell;
}
//ДКП-141п
//

- (void)viewDidLoad {
    [super viewDidLoad];

    // Do any additional setup after loading the view.
    
    user = [NSUserDefaults standardUserDefaults];

    
    [self.raspUpTable setDelegate:self];
    [self.raspUpTable setDataSource:self];
    
    [self.raspLowTable setDelegate:self];
    [self.raspLowTable setDataSource:self];
    
//    self.raspUpTable.rowHeight = 125.f;
//    self.raspLowTable.rowHeight = 125.f;
    
    self.raspUpTable.backgroundColor = [UIColor colorWithRed:155.f green:195.f blue:76.f alpha:1];
    self.raspLowTable.backgroundColor = [UIColor colorWithRed:84.f green:146.f blue:242.f alpha:1];
    
    if (self.raspUpTable) {
        self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:155.f green:195.f blue:76.f alpha:1];

    }
    
    
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if (revealViewController) {
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    
    CalendarAndDate *calendar = [[CalendarAndDate alloc] init];
    arrayWithDates = [calendar fillDates];
    currentDay = [calendar determineCurrentDayOfWeek];
    if ([calendar isUpWeekWithCurrentDate]) {
        [self.raspLowTable scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:currentDay] atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
    } else {
        [self.raspUpTable scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:currentDay] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
    
//    UIScreenEdgePanGestureRecognizer *panEdgeScreenGestureRecognizer = [[UIScreenEdgePanGestureRecognizer alloc] initWithTarget:self action:@selector(actionFroGestureRecognizer)];
//    [self.raspUpTable addGestureRecognizer:panEdgeScreenGestureRecognizer];
//    [self.raspLowTable addGestureRecognizer:panEdgeScreenGestureRecognizer];
    
//    UIPanGestureRecognizer *panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(actionFroGestureRecognizer)];
//    [self.raspUpTable addGestureRecognizer:panGestureRecognizer];
    


    
}



- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 6;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSMutableDictionary *resultWeek;
    if ([self.raspUpTable isEqual:tableView]) {
        resultWeek = [user objectForKey:@"raspUpWeek"];
    } else {
        resultWeek = [user objectForKey:@"raspDownWeek"];
    }
    int counter = [[resultWeek objectForKey:[NSString stringWithFormat:@"%d", indexPath.section]] count];
    if (!counter) {
        return 1.f;
    } else {
        return 125.f;
    }

}




-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    NSDictionary *dict;
    if ([tableView isEqual:self.raspUpTable]) {
        
        dict = [user objectForKey:@"raspUpWeek"];
    } else {
        
        dict = [user objectForKey:@"raspDownWeek"];
    }
    NSInteger rows = [[dict objectForKey:[NSString stringWithFormat:@"%d", section]] count];
    if (rows == 0) {
        rows = 1;
        flagNullCell = 1;
    }
    return rows;

}



-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if ([tableView isEqual:self.raspUpTable]) {
        

        NSMutableDictionary *resultWeek = [user objectForKey:@"raspUpWeek"];
        int counter = [[resultWeek objectForKey:[NSString stringWithFormat:@"%d", indexPath.section]] count];
        if (!counter) {
            NSLog(@"vsdj");
            
            static NSString *simpleTableIndetfier = @"NullCell";
            NullTableViewCell *cell = (NullTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIndetfier];
            if (!cell) {
                cell = [[NullTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIndetfier];
            }
            
            flagNullCell = 0;
            return cell;
        }
        static NSString *simpleTableIndetfier = @"Subject";
        SubjTableViewCell *cell = (SubjTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIndetfier];
        if (!cell) {
            cell = [[SubjTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIndetfier];
            NSLog(@"entry!!");
        }
        NSMutableDictionary *resultDay = [resultWeek valueForKey:[NSString stringWithFormat:@"%ld", (long)indexPath.section]];
        NSMutableDictionary *result = [resultDay valueForKey:[NSString stringWithFormat:@"%ld", (long)indexPath.row]];
        NSString *time = [result valueForKey:@"TimeName"];
        NSArray *timeArray = [time componentsSeparatedByString:@"-"];
        [cell.timeBeginLabel setText:timeArray[0]];
        [cell.timeEndLabel setText:timeArray[1]];
        [cell.subjectLabel setText:[result valueForKey:@"SubjectName"]];
        [cell.lectureLabel setText:[result valueForKey:@"LectureName"]];
        NSString *classroom = [result valueForKey:@"ClassroomName"];
        classroom = [classroom stringByAppendingString:@"/"];
        classroom = [classroom stringByAppendingString:[result valueForKey:@"BuildingName"]];
        [cell.classroomLabel setText:classroom];
        [cell.typeLabel setText:[result valueForKey:@"TypeName"]];

        
        return cell;
    } else {
    

        NSMutableDictionary *resultWeek = [user valueForKey:@"raspDownWeek"];
        int counter = [[resultWeek objectForKey:[NSString stringWithFormat:@"%d", indexPath.section]] count];
        if (!counter) {
            NSLog(@"vsdj");
            
            static NSString *simpleTableIndetfier = @"NullCell";
            NullTableViewCell *cell = (NullTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIndetfier];
            if (!cell) {
                cell = [[NullTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIndetfier];
            }
            return cell;
        }
        static NSString *simpleTableIndetfier = @"Subject";
        SubjTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIndetfier];
        if (!cell) {
            cell = [[SubjTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIndetfier];
            return cell;
        }
        NSMutableDictionary *resultDay = [resultWeek valueForKey:[NSString stringWithFormat:@"%ld", (long)indexPath.section]];
        NSMutableDictionary *result = [resultDay objectForKey:[NSString stringWithFormat:@"%ld", (long)indexPath.row]];
        NSString *time = [result valueForKey:@"TimeName"];
        NSArray *timeArray = [time componentsSeparatedByString:@"-"];
        [cell.timeBeginLabel setText:timeArray[0]];
        [cell.timeEndLabel setText:timeArray[1]];
        [cell.subjectLabel setText:[result valueForKey:@"SubjectName"]];
        [cell.lectureLabel setText:[result valueForKey:@"LectureName"]];
        NSString *classroom = [result valueForKey:@"ClassroomName"];
        classroom = [classroom stringByAppendingString:@"/"];
        classroom = [classroom stringByAppendingString:[result valueForKey:@"BuildingName"]];
        [cell.classroomLabel setText:classroom];
        [cell.typeLabel setText:[result valueForKey:@"TypeName"]];
        
        return cell;
    
    }
    

}

#pragma mark - Header and footer

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {

    UIImageView *img = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"headerNew@2x"]];

    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), CGRectGetHeight(img.bounds))];
    headerView.backgroundColor = [UIColor clearColor];
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(20, 30, self.view.bounds.size.width - 40, 50)];
    title.layer.borderColor = [UIColor blackColor].CGColor;
//    title.layer.borderWidth = 2.f;
    title.textAlignment = NSTextAlignmentCenter;
    
    [UIFont fontWithName:@"HelveticaNeue" size:36];
    [title setFont:[UIFont systemFontOfSize:36]];
    
    [img addSubview:title];
    title.text = [self getTitleForSection:section with:tableView];
    title.font = [UIFont fontWithName:@"RobotoCondensed-Regular" size:24];
    title.textAlignment = NSTextAlignmentLeft;
    if ([tableView isEqual:self.raspUpTable]) {
        title.textColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"green"]];
    } else {
        title.textColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"blue"]];
    }
    [headerView addSubview:img];
    return img;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 80;
}



-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    
    UIImageView *footerImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"footer"]];
    return footerImg;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 36.f;
}


- (NSString *) getTitleForSection:(NSInteger)section with:(UITableView *)tableView {
    int index = section;
    if ([tableView isEqual:self.raspLowTable]) {
        index += 6;
    }
    return arrayWithDates[index];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}




#pragma mark - Actions

- (IBAction)menuButtonAction:(UIButton *)sender {
    
    [self.revealViewController revealToggleAnimated:YES];
    NSLog(@"Button recognized!");

}

- (void)actionFroGestureRecognizer {
    
    NSLog(@"Gesture recognized!");
    
    MainViewController *action = [[MainViewController alloc] init];
    [action revealMenu];

    [self.revealViewController revealToggle:self];

}
@end
