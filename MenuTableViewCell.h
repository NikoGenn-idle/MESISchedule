//
//  MenuTableViewCell.h
//  MESISchedule
//
//  Created by Reutskiy Jury on 21.04.15.
//  Copyright (c) 2015 MESI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *iconImg;
@property (weak, nonatomic) IBOutlet UILabel *menuItemLabel;

@end
