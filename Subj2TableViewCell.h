//
//  Subj2TableViewCell.h
//  MESISchedule
//
//  Created by Виктор Киселев on 05.03.15.
//  Copyright (c) 2015 MESI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Subj2TableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *time2Label;

@property (strong, nonatomic) IBOutlet UILabel *subject2Label;

@property (strong, nonatomic) IBOutlet UILabel *lecture2Label;

@property (strong, nonatomic) IBOutlet UILabel *classroom2Label;

@property (strong, nonatomic) IBOutlet UILabel *type2Label;

@end
