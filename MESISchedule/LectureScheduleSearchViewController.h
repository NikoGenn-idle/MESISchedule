//
//  LectureScheduleSearchViewController.h
//  MESISchedule
//
//  Created by Reutskiy Jury on 05.05.15.
//  Copyright (c) 2015 MESI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LectureScheduleSearchViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate>


@end
