//
//  LectureSearchTableViewCell.m
//  MESISchedule
//
//  Created by Reutskiy Jury on 05.05.15.
//  Copyright (c) 2015 MESI. All rights reserved.
//

#import "LectureSearchTableViewCell.h"

@implementation LectureSearchTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
