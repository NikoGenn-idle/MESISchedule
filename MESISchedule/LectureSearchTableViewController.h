//
//  LectureSearchTableViewController.h
//  MESISchedule
//
//  Created by Reutskiy Jury on 08.05.15.
//  Copyright (c) 2015 MESI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LectureSearchTableViewController : UITableViewController <UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UIBarButtonItem *menuButton;
@property (strong, nonatomic) IBOutlet UITableView *table;


@end
