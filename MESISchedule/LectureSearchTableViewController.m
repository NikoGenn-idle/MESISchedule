//
//  LectureSearchTableViewController.m
//  MESISchedule
//
//  Created by Reutskiy Jury on 08.05.15.
//  Copyright (c) 2015 MESI. All rights reserved.
//

#import "LectureSearchTableViewController.h"
#import "SWRevealViewController.h"
#import "ServMethodClass.h"
#import "LectureSearchTableViewCell.h"

@interface LectureSearchTableViewController ()

@end

@implementation LectureSearchTableViewController {
    NSUserDefaults *user;
    NSArray *allTeachers;
    NSMutableArray *searchResults;
}

-(void)viewWillAppear:(BOOL)animated {
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"blueBackground@2x"] forBarMetrics:UIBarMetricsDefault];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.table.delegate = self;
    self.table.dataSource = self;
    
    NSShadow *shadow = [NSShadow new];
    NSDictionary *attributesNavBarTitle = @{
                                            NSForegroundColorAttributeName: [UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0],
                                            NSShadowAttributeName: shadow,
                                            NSFontAttributeName: [UIFont fontWithName:@"Roboto-Regular" size:18.0]
                                            };
    
    
    self.navigationController.navigationBar.titleTextAttributes = attributesNavBarTitle;
    
    user = [NSUserDefaults standardUserDefaults];
    allTeachers = [user objectForKey:@"Teachers"];
    searchResults = [allTeachers mutableCopy];
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController ) {
        [self.menuButton setTarget: self.revealViewController];
        [self.menuButton setAction: @selector(revealToggle:)];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }


}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return [searchResults count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"LectureCell";
    LectureSearchTableViewCell *cell = (LectureSearchTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[LectureSearchTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    cell.lectureLabel.text = searchResults[indexPath.row];
    return cell;
}

#pragma mark - Segue

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_USEC)), dispatch_get_main_queue(), ^{
        
        NSString *lecture = searchResults[indexPath.row];
        ServMethodClass *serv = [[ServMethodClass alloc] init];
        NSDictionary *dict = [serv requstLecture:lecture];
        if ([dict objectForKey:@"Error"]) {
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Ошибка" message:[dict valueForKey:@"Error"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        } else {
            [user setObject:dict[@"0"] forKey:@"Lecture up schedule"];
            [user setObject:dict[@"1"] forKey:@"Lecture down schedule"];
            [self performSegueWithIdentifier:@"FromLectureScheduleToMain" sender:self];
        }
    });
    
    
    //    Product *product = [self.searchResults objectAtIndex:indexPath.row];
    //    DetailViewController *vc = [[self storyboard] instantiateViewControllerWithIdentifier:@"DetailViewController"];
    //    self.presentingViewController.navigationItem.title = @"Search";
    //    vc.product = product;
    //    [self.presentingViewController.navigationController pushViewController:vc animated:YES];
}

#pragma mark - UISearchBar Delegate

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    NSLog(@"text = %@", searchText);
    NSLog(@"first object 1 = %@", [searchResults firstObject]);
    [self updateFilteredContentForTeacherName:searchText];
    [self.table reloadData];
    NSLog(@"first object 2 = %@", [searchResults firstObject]);
}


#pragma mark - Content Filtering

- (void)updateFilteredContentForTeacherName:(NSString *)teacherName {
    
    // Update the filtered array based on the search text and scope.
    if ((teacherName == nil) || [teacherName length] == 0) {
        // If there is no search string and the scope is "All".
        
        searchResults = [allTeachers mutableCopy];
        
        return;
    }

    [searchResults removeAllObjects]; // First clear the filtered array.
    
    //  Search the main list for products whose type matches the scope (if selected) and whose name matches searchText; add items that match to the filtered array.
    for (NSString *teacher in allTeachers) {
        NSUInteger searchOptions = NSCaseInsensitiveSearch | NSAnchoredSearch | NSDiacriticInsensitiveSearch;
        NSRange teacherNameRange = NSMakeRange(0, teacher.length);
        NSRange foundRange = [teacher rangeOfString:teacherName options:searchOptions range:teacherNameRange];
        if (foundRange.length > 0) {
            [searchResults addObject:teacher];
        }
    }
}


@end
