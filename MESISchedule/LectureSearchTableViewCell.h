//
//  LectureSearchTableViewCell.h
//  MESISchedule
//
//  Created by Reutskiy Jury on 05.05.15.
//  Copyright (c) 2015 MESI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LectureSearchTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lectureLabel;

@end
