//
//  AppDelegate.h
//  MESISchedule
//
//  Created by NikoGenn on 19.02.15.
//  Copyright (c) 2015 MESI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

// АЭМ-1402
// ДКП-141п
// ЯКТ-1415
// ДКП-142б