//
//  LectureScheduleViewController.m
//  MESISchedule
//
//  Created by Reutskiy Jury on 27.04.15.
//  Copyright (c) 2015 MESI. All rights reserved.
//

#import "LectureScheduleViewController.h"
#import "SWRevealViewController.h"
#import "ServMethodClass.h"
#import "LectureSearchTableViewCell.h"
#import "SearchResultTableViewController.h"



@interface LectureScheduleViewController ()


@property (nonatomic, strong) UISearchController *searchController;


@end



@implementation LectureScheduleViewController {
    NSUserDefaults *user;
    NSArray *allTeachers;
    NSMutableArray *searchResults;
}

-(void)viewWillAppear:(BOOL)animated {
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"blueBackground@2x"] forBarMetrics:UIBarMetricsDefault];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.table.delegate = self;
    self.table.dataSource = self;
    
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
    });
    
    NSShadow *shadow = [NSShadow new];
    NSDictionary *attributesNavBarTitle = @{
                                 NSForegroundColorAttributeName: [UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0],
                                 NSShadowAttributeName: shadow,
                                 NSFontAttributeName: [UIFont fontWithName:@"Roboto-Regular" size:18.0]
                                 };

    
    self.navigationController.navigationBar.titleTextAttributes = attributesNavBarTitle;
    
    user = [NSUserDefaults standardUserDefaults];
    allTeachers = [user objectForKey:@"Teachers"];
    searchResults = [user objectForKey:@"Teachers"];
    NSLog(@"%@", searchResults);
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController ) {
        [self.menuButton setTarget: self.revealViewController];
        [self.menuButton setAction: @selector(revealToggle:)];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }

    // Create a mutable array to contain products for the search results table.
//    self.searchResults = [NSMutableArray arrayWithCapacity:[allTeachers count]];
    
    // The table view controller is in a nav controller, and so the containing nav controller is the 'search results controller'

    
//    UINavigationController *searchResultsController = [[self storyboard] instantiateViewControllerWithIdentifier:@"TableSearchResultsNavController"];
//    
//    self.searchController = [[UISearchController alloc] initWithSearchResultsController:searchResultsController];
//    
//    self.searchController.searchBar.delegate = self;
//    
//    self.searchController.searchResultsUpdater = self;
//    
//    self.searchController.searchBar.frame = CGRectMake(self.searchController.searchBar.frame.origin.x, self.searchController.searchBar.frame.origin.y, self.searchController.searchBar.frame.size.width, 44.0);
//    
//    self.table.tableHeaderView = self.searchController.searchBar;
//    
//    self.definesPresentationContext = YES;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Segue

- (void)confirmAndSegue {
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_USEC)), dispatch_get_main_queue(), ^{
//
//    NSString *lecture = self.lectureTextField.text;
//    ServMethodClass *serv = [[ServMethodClass alloc] init];
//    NSDictionary *dict = [serv requstLecture:lecture];
//    if ([dict objectForKey:@"Error"]) {
//        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Ошибка" message:[dict valueForKey:@"Error"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
//        [alert show];
//    } else {
//        [user setObject:dict[@"0"] forKey:@"Lecture up schedule"];
//        [user setObject:dict[@"1"] forKey:@"Lecture down schedule"];
//        [self performSegueWithIdentifier:@"FromLectureScheduleToMain" sender:self];
//    }
//    });
}

#pragma mark - Table

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [searchResults count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"LectureCell";
    LectureSearchTableViewCell *cell = (LectureSearchTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[LectureSearchTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    cell.lectureLabel.text = searchResults[indexPath.row];
//    NSLog(@"Cell = %@", searchResults[indexPath.row]);
    return cell;
}

#pragma mark - UISearchBarDelegate

// Workaround for bug: -updateSearchResultsForSearchController: is not called when scope buttons change
//-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
//    NSString *string = searchBar.text;
//    NSLog(@"we finished with %@", string);
//    
//}

#pragma mark - UISearchResultsUpdating

-(void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    
    NSString *searchString = [self.searchController.searchBar text];
    NSLog(@"updateSearchResultsForSearchController with string %@", searchString);
    [self updateFilteredContentForTeacherName:searchString];
    [self.table reloadData];
    
    
    //    NSString *scope = nil;
    //
    //    NSInteger selectedScopeButtonIndex = [self.searchController.searchBar selectedScopeButtonIndex];
    //    if (selectedScopeButtonIndex > 0) {
    //        scope = [[Product deviceTypeNames] objectAtIndex:(selectedScopeButtonIndex - 1)];
    //    }
    
//    [self updateFilteredContentForTeacherName:searchString];
    
//    if (self.searchController.searchResultsController) {
//        UINavigationController *navController = (UINavigationController *)self.searchController.searchResultsController;
//        
//        SearchResultTableViewController *vc = (SearchResultTableViewController *)navController.topViewController;
//        [user setObject:searchResults forKey:@"search"];
////        vc.searchResultsArray = self.searchResults;
//        [vc.tableView reloadData];
//    }
    
}


#pragma mark - Content Filtering

- (void)updateFilteredContentForTeacherName:(NSString *)teacherName {
    
    // Update the filtered array based on the search text and scope.
    if ((teacherName == nil) || [teacherName length] == 0) {
        // If there is no search string and the scope is "All".

        searchResults = [allTeachers mutableCopy];

        return;
    }
    
    
    [searchResults removeAllObjects]; // First clear the filtered array.
    
    //  Search the main list for products whose type matches the scope (if selected) and whose name matches searchText; add items that match to the filtered array.
    for (NSString *teacher in allTeachers) {
        NSUInteger searchOptions = NSCaseInsensitiveSearch | NSDiacriticInsensitiveSearch;
        NSRange teacherNameRange = NSMakeRange(0, teacher.length);
        NSRange foundRange = [teacher rangeOfString:teacherName options:searchOptions range:teacherNameRange];
        if (foundRange.length > 0) {
            [searchResults addObject:teacher];
        }
    }
}




@end
