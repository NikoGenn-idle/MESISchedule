//
//  MainOtherGroupViewController.m
//  MESISchedule
//
//  Created by Reutskiy Jury on 03.05.15.
//  Copyright (c) 2015 MESI. All rights reserved.
//

#import "MainOtherGroupViewController.h"
#import "SWRevealViewController.h"
#import "RaspOtherGroupViewController.h"


@interface MainOtherGroupViewController () {
    
    NSInteger index;
    NSInteger countIndexPage;
}

@end

@implementation MainOtherGroupViewController


-(void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];

    
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController ) {
        [self.barButton setTarget: self.revealViewController];
        [self.barButton setAction: @selector(revealToggle:)];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    
    [self.selectedWeekPageViewController setDataSource:self];
    
    
    
    
    self.selectedWeekPageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageViewController"];
    
    self.selectedWeekPageViewController.dataSource = self;
    
    index = 0;
    countIndexPage = 0;
    
    //    [self.selectedWeekPageViewController initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationVertical options:nil];
    
    
    
    //    RaspViewController *startingViewController = [self viewControllerAtIndex:0];
    //    RaspViewController *secondViewController = [self viewControllerAtIndex:1];
    
    RaspOtherGroupViewController *startingViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"UpWeekOtherGroup"];
    
    NSArray *viewControllers = @[startingViewController];
    
    
    [self.selectedWeekPageViewController setViewControllers:viewControllers  direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    
    self.selectedWeekPageViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    
    //    [self addChildViewController:_SelectWeekPageViewController];
    
    [self.view addSubview:self.selectedWeekPageViewController.view];
    self.view.alpha = 1;
    //    [self.SelectWeekPageViewController didMoveToParentViewController:self];
    
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}





#pragma mark - Page View Controller Data Source




- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    
    
    
    
    //    NSLog(@"Before begin with index = %d, and count = %d", index, countIndexPage);
    
    if (countIndexPage < 2) {
        countIndexPage++;
    } else {
        index = 0;
    }
    if (index == 1) {
        
        RaspOtherGroupViewController *UpWeekViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"UpWeekOtherGroup"];
        
        return UpWeekViewController;
        
        
    } else {
        //        NSLog(@"Before end with index = %d, and count = %d return NIL", index, countIndexPage);
        
        return nil;
    }
    
}



- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
    
    
    
    //    NSLog(@"After begin with index = %d, and count] = %d", index, countIndexPage);
    
    if (countIndexPage < 2) {
        countIndexPage++;
    } else {
        index = 1;
    }
    
    if (index == 0) {
        
        //        if (countIndexPage <= 2) {
        //            countIndexPage++;
        //        } else {
        //            index = 1;
        //        }
        
        RaspOtherGroupViewController *DownWeekViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"DownWeekOtherGroup"];

        
        return DownWeekViewController;
        
    } else {
        
        return nil;
    }
}







-(NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController{
    return 0;
}
-(NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController{
    return 0;
}


@end
