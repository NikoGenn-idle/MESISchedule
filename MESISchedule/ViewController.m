//
//  ViewController.m
//  MESISchedule
//
//  Created by NikoGenn on 19.02.15.
//  Copyright (c) 2015 MESI. All rights reserved.
//

#import "ViewController.h"
#import "ServMethodClass.h"
#import "MainViewController.h"

@interface ViewController () {
    NSUserDefaults *user;
    NSMutableDictionary *dict;
}

@end

@implementation ViewController

//-(void)viewWillAppear:(BOOL)animated {
//    if ([user objectForKey:@"raspUpWeek"]) {
//        [self performSegueWithIdentifier:@"Main" sender:self];
//    }
//    NSLog(@"segue!");
//    [self performSegueWithIdentifier:@"Main" sender:self];
//
//
//}
- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:YES];
    user = [NSUserDefaults standardUserDefaults];
    NSLog(@"%@", user);

    
    
    if ([user valueForKey:@"raspUpWeek"]) {
        
        NSLog(@"iukjn");
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.0001 * NSEC_PER_USEC)), dispatch_get_main_queue(), ^{
            [self performSegueWithIdentifier:@"passReg1" sender:self.view];
        });

        NSLog(@"iukjn");
        
        //        MainViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"mainView"];
        //        [self.navigationController pushViewController:controller animated:YES];
        
    } else {
        NSLog(@"don't work");
    }

}

- (void)viewDidLoad {
    [super viewDidLoad];
    

    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
    self.Group.delegate = self;
    self.secondGroup.delegate = self;
    self.thirdGroup.delegate = self;
    
//    user = [NSUserDefaults standardUserDefaults];
//    NSLog(@"%@", user);
//    if (user) {
//        
//        NSLog(@"iukjn");
//        [self performSegueWithIdentifier:@"passReg" sender:self.view];
//        [self prepareForSegue:@"ScreenMain" sender:self];
//        NSLog(@"iukjn");
//
////        MainViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"mainView"];
////        [self.navigationController pushViewController:controller animated:YES];
//        
//    } else {
//        NSLog(@"don't work");
//    }
    
   [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"SBPahBE-o50.jpg"]]];
    _logoImage.image = [UIImage imageNamed:@"logo.png"];
    
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//ДКИ-142б

- (IBAction)confirm:(id)sender {
    ServMethodClass *serv = [ServMethodClass alloc];
    dict = [[NSMutableDictionary alloc] init];
    dict = [serv requestFirstGroup:self.Group.text secondGroup:self.secondGroup.text thirdGroup:self.thirdGroup.text];
    
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        
//        if ([dict valueForKey:@"Error"]){
//            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Ошибка" message:[dict valueForKey:@"Error"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
//            [alert show];
//        }
//        else {
            NSDictionary *upWeek = [[NSDictionary alloc] init];
            NSDictionary *downWeek = [[NSDictionary alloc] init];
            upWeek = [NSMutableDictionary dictionaryWithDictionary:[dict valueForKey:[NSString stringWithFormat:@"0"]]];
            downWeek = [NSMutableDictionary dictionaryWithDictionary:[dict valueForKey:[NSString stringWithFormat:@"1"]]];
            NSLog(@"up week = %@", upWeek);
            NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
            [user setObject:upWeek forKey:@"raspUpWeek"];
            [user setObject:downWeek forKey:@"raspDownWeek"];
            
            [self performSegueWithIdentifier:@"passReg1" sender:self.view];
            NSLog(@"button action");
            
//        }
    });
}



- (void)showLogs {

    NSLog(@"1");
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(10 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        NSLog(@"2");
        
    });
    NSLog(@"3");
}


-(void)dismissKeyboard {
    if ([self.Group isFirstResponder]) {
        [self.Group resignFirstResponder];
    } else if ([self.secondGroup isFirstResponder]) {
        [self.secondGroup resignFirstResponder];
    } else if ([self.thirdGroup isFirstResponder]) {
        [self.thirdGroup resignFirstResponder];
    }
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.Group) {
        [self.Group resignFirstResponder];
        [self.secondGroup becomeFirstResponder];
    }
    else {
        [textField resignFirstResponder];
    }
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    NSLog(@"kuku");
    if (textField != self.Group && [self.Group.text  isEqual: @""]) {
        return NO;
    }
    
    return YES;
}



@end
