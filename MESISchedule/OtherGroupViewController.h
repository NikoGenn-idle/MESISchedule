//
//  OtherGroupViewController.h
//  MESISchedule
//
//  Created by Reutskiy Jury on 27.04.15.
//  Copyright (c) 2015 MESI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OtherGroupViewController : UIViewController


@property (weak, nonatomic) IBOutlet UIBarButtonItem *menuButton;
@property (weak, nonatomic) IBOutlet UITextField *groupTextField;
- (IBAction)button:(UIButton *)sender;
- (IBAction)endEditingText:(UITextField *)sender;


@end
