//
//  SearchResultTableViewController.m
//  MESISchedule
//
//  Created by Reutskiy Jury on 06.05.15.
//  Copyright (c) 2015 MESI. All rights reserved.
//

#import "SearchResultTableViewController.h"
#import "LectureScheduleViewController.h"
#import "LectureSearchTableViewCell.h"
#import "ServMethodClass.h"


@interface SearchResultTableViewController ()



@end


@implementation SearchResultTableViewController {

    NSUserDefaults *user;
    NSArray *allTeachers;
    NSArray *searchResultsArray;

}
@synthesize searchResultsArray;

- (void)viewDidLoad {
    [super viewDidLoad];

    user = [NSUserDefaults standardUserDefaults];
    allTeachers = [user objectForKey:@"Trachers"];

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSLog(@"here!!");

    return [self.searchResultsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"LectureResultCell";
    LectureSearchTableViewCell *cell = (LectureSearchTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[LectureSearchTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    cell.lectureLabel.text = allTeachers[indexPath.row];
    
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_USEC)), dispatch_get_main_queue(), ^{
    
        NSString *lecture = allTeachers[indexPath.row];
        ServMethodClass *serv = [[ServMethodClass alloc] init];
        NSDictionary *dict = [serv requstLecture:lecture];
        if ([dict objectForKey:@"Error"]) {
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Ошибка" message:[dict valueForKey:@"Error"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        } else {
            [user setObject:dict[@"0"] forKey:@"Lecture up schedule"];
            [user setObject:dict[@"1"] forKey:@"Lecture down schedule"];
            [self performSegueWithIdentifier:@"FromLectureScheduleToMain" sender:self];
        }
        });
    
    
//    Product *product = [self.searchResults objectAtIndex:indexPath.row];
//    DetailViewController *vc = [[self storyboard] instantiateViewControllerWithIdentifier:@"DetailViewController"];
//    self.presentingViewController.navigationItem.title = @"Search";
//    vc.product = product;
//    [self.presentingViewController.navigationController pushViewController:vc animated:YES];
}


@end
