//
//  SearchResultTableViewController.h
//  MESISchedule
//
//  Created by Reutskiy Jury on 06.05.15.
//  Copyright (c) 2015 MESI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchResultTableViewController : UITableViewController

@property (nonatomic, strong) NSArray *searchResultsArray;


@end
