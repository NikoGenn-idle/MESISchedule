//
//  OtherGroupViewController.m
//  MESISchedule
//
//  Created by Reutskiy Jury on 27.04.15.
//  Copyright (c) 2015 MESI. All rights reserved.
//

#import "OtherGroupViewController.h"
#import "SWRevealViewController.h"
#import "ServMethodClass.h"

@interface OtherGroupViewController ()

@end

@implementation OtherGroupViewController {
    NSUserDefaults *user;
    NSMutableDictionary *lastGroups;

}

-(void)viewWillAppear:(BOOL)animated {
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"blueBackground@2x"] forBarMetrics:UIBarMetricsDefault];

}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    user = [NSUserDefaults standardUserDefaults];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"blueBackground@2x"] forBarMetrics:UIBarMetricsDefault];
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController ) {
        [self.menuButton setTarget: self.revealViewController];
        [self.menuButton setAction: @selector(revealToggle:)];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
 
    NSShadow *shadow = [NSShadow new];
    NSDictionary *attributes = @{
                                 NSForegroundColorAttributeName: [UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0],
                                 NSShadowAttributeName: shadow,
                                 NSFontAttributeName: [UIFont fontWithName:@"Roboto-Regular" size:20.0]
                                 };
    
    NSDictionary *attributesTextField =@{
                                         NSForegroundColorAttributeName: [UIColor colorWithRed:114.0/255.0 green:114.0/255.0 blue:114.0/255.0 alpha:1.0],
                                         NSShadowAttributeName: shadow,
                                         NSFontAttributeName: [UIFont fontWithName:@"Roboto-Light" size:16.0]
                                         };
    
    self.navigationController.navigationBar.titleTextAttributes = attributes;
    self.groupTextField.defaultTextAttributes = attributesTextField;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Segue and work with data

- (void) confirmAndSegue {
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_USEC)), dispatch_get_main_queue(), ^{

    ServMethodClass *serv = [[ServMethodClass alloc] init];
    NSString *group = self.groupTextField.text;
    NSDictionary *dict = [serv requestFirstGroup:group secondGroup:@"" thirdGroup:@""];
    if ([dict objectForKey:@"Error"]) {
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Ошибка" message:[dict valueForKey:@"Error"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    } else {
        [user setObject:dict[@"0"] forKey:@"Other group up"];
        [user setObject:dict[@"1"] forKey:@"Other group down"];

        [self refreshListOfLastGroupsWith:group];
        [self performSegueWithIdentifier:@"FromOtherGroupToMainView" sender:self];
    }
    });
}

- (void) refreshListOfLastGroupsWith:(NSString*)group {
    if (![user objectForKey:@"Last groups"]) {
        lastGroups = [[NSMutableDictionary alloc] init];
        [user setObject:lastGroups forKey:@"Last groups"];
    } else {
        lastGroups = [user objectForKey:@"Last groups"];
    }
    if (lastGroups.count < 5) {
        NSString *numberGroupInArray = [NSString stringWithFormat:@"%d", lastGroups.count];
        [lastGroups setObject:group forKey:numberGroupInArray];
    } else {
        [lastGroups removeObjectForKey:@"1"];
        for (int i = 2; i < 6; i++) {
            NSString *tempString = [lastGroups objectForKey:[NSString stringWithFormat:@"%d", i]];
            [lastGroups setObject:tempString forKey:[NSString stringWithFormat:@"%d", i - 1]];
        }
        [lastGroups setObject:group forKey:[NSString stringWithFormat:@"5"]];
    }
    [user removeObjectForKey:@"Last groups"];
    [user setObject:lastGroups forKey:@"Last groups"];
    
}

#pragma mark - Actions

- (IBAction)button:(UIButton *)sender {
    [self confirmAndSegue];

}

- (IBAction)endEditingText:(UITextField *)sender {
    [self confirmAndSegue];

}





@end
