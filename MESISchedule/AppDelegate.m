//
//  AppDelegate.m
//  MESISchedule
//
//  Created by NikoGenn on 19.02.15.
//  Copyright (c) 2015 MESI. All rights reserved.
//

#import "AppDelegate.h"
#import "CalendarAndDate.h"
#import "ServMethodClass.h"
#import "SearchResultTableViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

    UIPageControl *pp = [UIPageControl appearance];
    pp.pageIndicatorTintColor = [UIColor lightGrayColor];
    pp.currentPageIndicatorTintColor = [UIColor blackColor];
    pp.backgroundColor = [UIColor whiteColor];
    
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    CalendarAndDate *object = [[CalendarAndDate alloc] init];
    [object setFirst:@"20:4:15"];
    ServMethodClass *serv = [[ServMethodClass alloc] init];
    if ([serv checkConnection]) {
        NSLog(@"have inet");

        NSDictionary *allLectures = [serv getListOfAll:lectures];
        NSLog(@"%@", [allLectures objectForKey:@"0"]);
        NSMutableArray *tempArray = [[NSMutableArray alloc] init];
        for (int i = 0; i < [allLectures count]; i++) {
            NSString *tempString = [allLectures objectForKey:[NSString stringWithFormat:@"%d", i]];
            tempArray[i] = tempString;
        }
        NSArray *teachersArray = tempArray;
        NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
        [user setObject:teachersArray forKey:@"Teachers"];
    } else {
        NSLog(@"No inet");
    }
   


    // Override point for customization after application launch.
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
