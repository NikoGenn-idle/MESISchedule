//
//  LectureScheduleViewController.h
//  MESISchedule
//
//  Created by Reutskiy Jury on 27.04.15.
//  Copyright (c) 2015 MESI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LectureScheduleViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate>

@property (weak, nonatomic) IBOutlet UIBarButtonItem *menuButton;
@property (weak, nonatomic) IBOutlet UITableView *table;
@property (weak, nonatomic) NSArray *lecturesArray;


@end
