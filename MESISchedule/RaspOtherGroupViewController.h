//
//  RaspOtherGroupViewController.h
//  MESISchedule
//
//  Created by Reutskiy Jury on 03.05.15.
//  Copyright (c) 2015 MESI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RaspOtherGroupViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView *raspUpTable;
@property (strong, nonatomic) IBOutlet UITableView *raspLowTable;



@end
