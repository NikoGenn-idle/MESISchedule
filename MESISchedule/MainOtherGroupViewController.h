//
//  MainOtherGroupViewController.h
//  MESISchedule
//
//  Created by Reutskiy Jury on 03.05.15.
//  Copyright (c) 2015 MESI. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface MainOtherGroupViewController : UIViewController <UIPageViewControllerDataSource, UIPageViewControllerDelegate>

@property NSUInteger pageIndex;
@property (strong, nonatomic) UIPageViewController *selectedWeekPageViewController;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *barButton;

@end
