//
//  TablesLectureScheduleViewController.h
//  MESISchedule
//
//  Created by Reutskiy Jury on 04.05.15.
//  Copyright (c) 2015 MESI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TablesLectureScheduleViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *raspUpTable;
@property (weak, nonatomic) IBOutlet UITableView *raspLowTable;

@end
