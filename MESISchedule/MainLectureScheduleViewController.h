//
//  MainLectureScheduleViewController.h
//  MESISchedule
//
//  Created by Reutskiy Jury on 04.05.15.
//  Copyright (c) 2015 MESI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainLectureScheduleViewController : UIViewController <UIPageViewControllerDataSource, UIPageViewControllerDelegate>

@property NSUInteger pageIndex;
@property (strong, nonatomic) UIPageViewController *selectedWeekPageViewController;

@end
