//
//  RaspOtherGroupViewController.m
//  MESISchedule
//
//  Created by Reutskiy Jury on 03.05.15.
//  Copyright (c) 2015 MESI. All rights reserved.
//

#import "RaspOtherGroupViewController.h"
#import "SubjTableViewCell.h"
#import "MainOtherGroupViewController.h"
#import "SWRevealViewController.h"


@interface RaspOtherGroupViewController ()

@end

@implementation RaspOtherGroupViewController {
    NSUserDefaults *user;
    UITableView *tableview;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    user = [NSUserDefaults standardUserDefaults];
    
    
    [self.raspUpTable setDelegate:self];
    [self.raspUpTable setDataSource:self];
    
    [self.raspLowTable setDelegate:self];
    [self.raspLowTable setDataSource:self];
    
    self.raspUpTable.rowHeight = 125.f;
    self.raspLowTable.rowHeight = 125.f;
    
    self.raspUpTable.backgroundColor = [UIColor colorWithRed:155.f green:195.f blue:76.f alpha:1];
    self.raspLowTable.backgroundColor = [UIColor colorWithRed:84.f green:146.f blue:242.f alpha:1];
    
    if (self.raspUpTable) {
        self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:155.f green:195.f blue:76.f alpha:1];
        
    }

}

-(void)viewDidDisappear:(BOOL)animated {
    [user removeObjectForKey:@"Other group up"];
    [user removeObjectForKey:@"Other group down"];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table

- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 6;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if ([tableView isEqual:self.raspUpTable]) {
        
        
        
        NSDictionary *dict = [user objectForKey:@"Other group up"];
        NSInteger rows = [[dict objectForKey:[NSString stringWithFormat:@"%d", section]] count];
        return rows;
    } else {
        NSDictionary *dict = [user objectForKey:@"Other group down"];
        //        NSLog(@"%@", dict);
        NSInteger rows = [[dict objectForKey:[NSString stringWithFormat:@"%d", section]] count];
        return rows;
    }
    
}



-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([tableView isEqual:self.raspUpTable]) {
        
        
        
        static NSString *simpleTableIndetfier = @"Subject";
        SubjTableViewCell *cell = (SubjTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIndetfier];
        if (!cell) {
            cell = [[SubjTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIndetfier];
            NSLog(@"entry!!");
            
        }
        //        [self drawRect:cell];
        NSLog(@"vsdj");
        NSMutableDictionary *resultWeek = [user objectForKey:@"Other group up"];
        //        NSLog(@"%@", resultWeek);
        NSMutableDictionary *resultDay = [resultWeek valueForKey:[NSString stringWithFormat:@"%ld", (long)indexPath.section]];
        NSMutableDictionary *result = [resultDay valueForKey:[NSString stringWithFormat:@"%ld", (long)indexPath.row]];
        //        NSLog(@"%@", result);
        //        [cell.timeLabel setText:[result valueForKey:@"TimeName"]];
        NSString *time = [result valueForKey:@"TimeName"];
        NSArray *timeArray = [time componentsSeparatedByString:@"-"];
        [cell.timeBeginLabel setText:timeArray[0]];
        [cell.timeEndLabel setText:timeArray[1]];
        [cell.subjectLabel setText:[result valueForKey:@"SubjectName"]];
        [cell.lectureLabel setText:[result valueForKey:@"LectureName"]];
        NSString *classroom = [result valueForKey:@"ClassroomName"];
        classroom = [classroom stringByAppendingString:@"/"];
        classroom = [classroom stringByAppendingString:[result valueForKey:@"BuildingName"]];
        [cell.classroomLabel setText:classroom];
        [cell.typeLabel setText:[result valueForKey:@"TypeName"]];
        
        
        return cell;
    } else {
        
        static NSString *simpleTableIndetfier = @"Subject";
        SubjTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIndetfier];
        if (!cell) {
            cell = [[SubjTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIndetfier];
            return cell;
        }
        //        [self drawRect:cell];
        NSMutableDictionary *resultWeek = [user valueForKey:@"Other group down"];
        NSMutableDictionary *resultDay = [resultWeek valueForKey:[NSString stringWithFormat:@"%ld", (long)indexPath.section]];
        NSMutableDictionary *result = [resultDay objectForKey:[NSString stringWithFormat:@"%ld", (long)indexPath.row]];
        //        NSLog(@"%@", result);
        NSString *time = [result valueForKey:@"TimeName"];
        NSArray *timeArray = [time componentsSeparatedByString:@"-"];
        [cell.timeBeginLabel setText:timeArray[0]];
        [cell.timeEndLabel setText:timeArray[1]];
        
        //        [cell.timeLabel setText:[result valueForKey:@"TimeName"]];
        [cell.subjectLabel setText:[result valueForKey:@"SubjectName"]];
        [cell.lectureLabel setText:[result valueForKey:@"LectureName"]];
        NSString *classroom = [result valueForKey:@"ClassroomName"];
        classroom = [classroom stringByAppendingString:@"/"];
        classroom = [classroom stringByAppendingString:[result valueForKey:@"BuildingName"]];
        [cell.classroomLabel setText:classroom];
        [cell.typeLabel setText:[result valueForKey:@"TypeName"]];
        
        return cell;
        
    }
    
    
}

#pragma mark - Header and footer

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UIImageView *img = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"headerNew@2x"]];
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), CGRectGetHeight(img.bounds))];
    headerView.backgroundColor = [UIColor clearColor];
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(20, 30, self.view.bounds.size.width - 40, 50)];
    title.layer.borderColor = [UIColor blackColor].CGColor;
    //    title.layer.borderWidth = 2.f;
    title.textAlignment = NSTextAlignmentCenter;
    
    [UIFont fontWithName:@"HelveticaNeue" size:36];
    [title setFont:[UIFont systemFontOfSize:36]];
    
    [img addSubview:title];
    title.text = [self getTitleForSection:section];
    title.font = [UIFont fontWithName:@"RobotoCondensed-Regular" size:24];
    title.textAlignment = NSTextAlignmentLeft;
    if ([tableView isEqual:self.raspUpTable]) {
        title.textColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"green"]];
    } else {
        title.textColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"blue"]];
    }
    [headerView addSubview:img];
    return img;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 80;
}



-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    
    UIImageView *footerImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"footer"]];
    return footerImg;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 36.f;
}


- (NSString *) getTitleForSection:(NSInteger)section {
    NSString *title;
    switch (section) {
        case 0:
            title = @"Понедельник";
            break;
        case 1:
            title = @"Вторник";
            break;
        case 2:
            title = @"Среда";
            break;
        case 3:
            title = @"Четверг";
            break;
        case 4:
            title = @"Пятница";
            break;
        case 5:
            title = @"Суббота";
            break;
        default:
            break;
    }
    return title;
}

@end
