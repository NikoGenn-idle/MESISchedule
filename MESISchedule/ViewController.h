//
//  ViewController.h
//  MESISchedule
//
//  Created by NikoGenn on 19.02.15.
//  Copyright (c) 2015 MESI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *Group;

@property (strong, nonatomic) IBOutlet UIButton *Confr;

@property (strong, nonatomic) IBOutlet UITextField *secondGroup;
@property (strong, nonatomic) IBOutlet UITextField *thirdGroup;

- (IBAction)confirm:(id)sender;

@property (strong, nonatomic) IBOutlet UIImageView *logoImage;


@end

