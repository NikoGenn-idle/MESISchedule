//
//  Rasp2ViewController.h
//  MESISchedule
//
//  Created by Виктор Киселев on 05.03.15.
//  Copyright (c) 2015 MESI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Rasp2ViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView *Rasp2Table;

@end
