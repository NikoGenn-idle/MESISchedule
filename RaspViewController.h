//
//  RaspViewController.h
//  MESISchedule
//
//  Created by Виктор Киселев on 19.02.15.
//  Copyright (c) 2015 MESI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RaspViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView *raspUpTable;
@property (strong, nonatomic) IBOutlet UITableView *raspLowTable;
@property (weak, nonatomic) IBOutlet UIButton *menuButtonUp;
@property (weak, nonatomic) IBOutlet UIButton *button;
@property (weak, nonatomic) IBOutlet UIButton *rightButton;

- (IBAction)menuButtonAction:(UIButton *)sender;


//- (BOOL) checkWeekTable;

@end
